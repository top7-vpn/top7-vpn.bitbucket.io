function getDHM(mililsec){

  var sec = (mililsec - (mililsec % 1000)) / 1000
  var min = (sec - sec % 60)/ 60
  var hrs = (min - min % 60) / 60
  var day = (hrs - hrs % 24) / 24

  return [sec % 60, min % 60, hrs % 24, day].map(function(el){ return ( el < 10 )? '0' + el : el;}).reverse();
}

function setTimeOnBanner(banner, timeArr) {
  var timeDiv = banner.getElementsByClassName('deal-counter');
  var spans = Array.from( timeDiv[0].getElementsByClassName('time') );
  spans[0].innerText = ( ( timeArr[0] ) );
  spans[1].innerText = ( ( timeArr[1] ) );
  spans[2].innerText = ( ( timeArr[2] ) );
  spans[3].innerText = ( ( timeArr[3] ) );
  return spans;
}


function openInNewTab(event, url, vendor = '') {
  event.preventDefault();
  event.stopPropagation();

  var rndClick = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();

  var deeplink = "";
  var posRegexp = /dl=(\w*)/g;
  var posNum = posRegexp.exec(url);
  if (posNum){
      deeplink=posNum[1];
  }
  var btnName = "";
  var regExpTarget = /target=([\w]*)&?/g;
  var targetBtn = regExpTarget.exec(url);
  if(targetBtn) {
      btnName = targetBtn[1];
  }

  // Check is the outong link has "vid" in it, if it is, send's it to Alooma
  var vid = "";
  var posRegexp = /vid=(\w*)/g;
  var posNum = posRegexp.exec(url);
  if (posNum){
    vid=posNum[1];
  }

  var aloomaClickData = {
      "pageview_id": rndPage,
      "clickout_id" : rndClick,
      "btnName" : btnName,
      "lable" : 'Deal Banner',
      "current_url" :window.location.href,
      "internalurl" : url,
      "deeplink": deeplink,
      "vendor_id" : vid,
  }

  if (vendor.length) {
    aloomaClickData.vendor = vendor;
  }

  aloomaClickData.splitCurrentUrl = {};
  aloomaClickData.splitCurrentUrl.host=window.location.origin + window.location.pathname;
  aloomaClickData.splitCurrentUrl.params=TrackingQueryString;
  // console.log('openInNewTab');

  if (url.indexOf('Track_link') > -1) {
    aloomaTrackPageview("clickout", aloomaClickData);
  } else {
    aloomaTrackPageview("clickin", aloomaClickData);
  }

  hideBanner();
  var win = window.open(url, '_blank');
  win.focus();
}

function vmHasActiveBanners() {
  var now = Date.now();
  var bfEndDate = Date.now();
  var currentBanner;
  var timeRemain = '';

  var bannerArray = document.getElementsByClassName("js-deal-wrap");
  for (var i = 0; i < bannerArray.length; i++) {
    var item = bannerArray[i];
    var start_date = parseInt(item.dataset.start_date) * 1000;
    var end_date = parseInt(item.dataset.end_date) * 1000;
    if (end_date < now || start_date > now) {
      continue;
    }

    var diffTime = end_date - now;

    if (!timeRemain || timeRemain > diffTime) {
      timeRemain = diffTime
      bfEndDate = end_date;
      currentBanner = item;

      if (getBannerEndTime(item.dataset.id, end_date) > now) {
        return currentBanner;
      }
    }
  }
}


function hideBanner() {

  if( !checkDealCookie('deal-closed') ) {
    setDealCookie( 'deal-closed', true, 1);
  }



  window.currentBanner.classList.add('visuallyhidden');

  window.currentBanner.addEventListener('transitionend', function (e) {
    window.currentBanner.classList.add('hidden');
    document.querySelector("body").classList.remove("has-deal-banner");

    var event = new Event("banner-closed", {bubbles: true}); // (2)
    document.dispatchEvent(event);

    if(window.stickySidebar &&  window.stickySidebar.style.top !== 'unset') {

      window.stickySidebar.style.top = '5px';
      window.deal.style.height = '0px';
      window.currentBanner.offsetHeight = 0;

      setTimeout(function() {
        window.deal.style.display = 'none';
      }, 150);
    }
  }, {
    capture: false,
    once: true,
    passive: false
  });
}

window.addEventListener('DOMContentLoaded', (event) => {

  var bannerArray = document.getElementsByClassName("js-deal-wrap");
  if( bannerArray.length > 0 ) {
    var now = Date.now();
    var bfEndDate = Date.now();
    var currentBanner;
    var timeRemain = '';

    if( checkDealCookie('deal-closed') ) {
        return;
    }
    if( typeof isNotOnBlogPage !== "undefined" && !isNotOnBlogPage ) {
      return;
    }
    // For the ABT - allow stop displaying the banner
    // can't do without jQuery (=, sorry Anton
    var load_banner = jQuery(document).triggerHandler( "vm/deal-banner/load", [] );
    if ( typeof load_banner !== "undefined" && !load_banner ) {
      return false;
    }

    for (var i = 0 ; i < bannerArray.length; i++) {
      var item = bannerArray[i];
      var start_date = parseInt(item.dataset.start_date) * 1000;
      var end_date =  parseInt(item.dataset.end_date) * 1000;
      if( end_date < now || start_date > now ) {
        continue;
      }

      var diffTime =  end_date - now;

      if( !timeRemain || timeRemain > diffTime ) {
        timeRemain = diffTime
        bfEndDate = end_date;
        currentBanner = item;

        if ( getBannerEndTime(item.dataset.id, end_date)> now ) {
          window.currentBanner = currentBanner;
          bfEndDate = getBannerEndTime(item.dataset.id, end_date);
        }

      }

    }
    if( typeof window.currentBanner == "undefined" ) {
      return;
    }

    setTimeOnBanner(window.currentBanner, getDHM( bfEndDate - now ));

  var timer = setInterval(function() {
    var diff = bfEndDate - Date.now();
    if ( diff > 0 ) {
      setTimeOnBanner(window.currentBanner, getDHM(diff));
    } else {
      document.querySelector(".js-deal-counter").style.visibility = "hidden";
      clearInterval();
    }
  }, 1000);


  currentBanner.classList.remove('hidden');
  setTimeout(function () {
    currentBanner.classList.remove('visuallyhidden');

    setTimeout(function () {
      // Fix for Vendor mostly
      if ( window.location.hash && window.innerWidth > 767 && jQuery(window.location.hash).length ) {
        window.scroll({
          top: jQuery(window.location.hash).offset().top - window.currentBanner.offsetHeight - 10,
          left: 0,
          behavior: 'smooth'
        });
      }
    }, 100);
  }, 20);

  window.deal  = document.querySelector(".js-deal-sticky");

  var sidebar_wrap  = document.getElementsByClassName("sticky-sidebar"),
    stickySidebar = '',
    docBody   = document.documentElement || document.body.parentNode || document.body,
    hasOffset = window.pageYOffset !== undefined,
    scrollTop;

  if(sidebar_wrap.length){
    window.stickySidebar = document.getElementById("sidebar");
  }

  document.querySelector("body").classList.add("has-deal-banner");

  window.onscroll = function (e) {
    // cross-browser compatible scrollTop.
    scrollTop = hasOffset ? window.pageYOffset : docBody.scrollTop;
    if ( window.currentBanner.classList.contains("deal-banner-old") || jQuery(window).width() > 767 ) {
      deal.style.height = currentBanner.offsetHeight + 'px';

      // if user scrolls to 60px from the top of the left div
      if (scrollTop >= deal.offsetTop) {
          if(typeof window.stickySidebar !== 'undefined' && window.stickySidebar.style.top == '5px') {
            window.stickySidebar.style.top = currentBanner.offsetHeight + 5 + 'px';
          }

          currentBanner.style.top = 0;
          currentBanner.style.zIndex = 998;
          currentBanner.style.position = 'fixed';
          document.querySelector("body").classList.add("has-deal-banner--fixed");
        } else {
          currentBanner.style.position = 'relative';
          currentBanner.style.zIndex = 'unset';
        }
      }
    }
    gtag('event', currentBanner.dataset.page_title, {
      'event_category': 'Deal '+ currentBanner.dataset.id + ' - show',
      'event_label': currentBanner.dataset.page_title
    });

    var aloomaClickData = {
      "pageview_id": rndPage,
      'event_action': currentBanner.dataset.page_title,
      'event_label': currentBanner.dataset.page_title,
    }

    aloomaTrackPageview('Deal '+ currentBanner.dataset.id + ' - show', aloomaClickData);
  }
});

function getBannerEndTime ( id, expiration) {
  if ( !expiration ) {
    expiration = 3600000 * 9 + (3600000 * 0.6565);
  }

  let now = Date.now();
  let localExp = getDealCookie( 'banner-' + id + '-expiration');
  if ( !localExp ) {
    localExp = expiration;
    setDealCookie('banner-' + id + '-expiration', localExp, 1);
  }

  return parseInt(localExp);
}

function closeBanner (event) {

  event.preventDefault();
  event.stopPropagation();

  gtag('event', currentBanner.dataset.page_title, {
    'event_category': 'Deal Exit Intent - Close Button X',
    'event_label': currentBanner.dataset.page_title
  });

  var aloomaClickData = {
    "pageview_id": rndPage,
    'event_action': currentBanner.dataset.page_title,
    'event_label': currentBanner.dataset.page_title,
  }

  aloomaTrackPageview('Deal Exit Intent - Close Button X', aloomaClickData);

  hideBanner();

}
function setDealCookie(cname, cvalue, exHours) {
  var d = new Date();
  d.setTime(d.getTime() + (exHours * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getDealCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return false;
}

function checkDealCookie(cname) {
  return !!getDealCookie(cname);
}


